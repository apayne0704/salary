#include <iostream>
#include <conio.h>
#include <iomanip> // gives us stream manipulators, specifically setw(int) and setprecision(int)

using namespace std;

struct employee
{
	// ID, First Name Last Name, Pay Rate, Hours
	int employeeID;
	string firstName;
	string lastName;
	float payRate;
	float hours;
};

int main()
{

	int arrSize;
	float grossPay = 0;
	float totalgrossPay = 0;
	// Declare initial column widths for cout display
	int colWidthID = 5;
	int colWidthFirstName = 11;
	int colWidthLastName = 10;
	int colWidthPayRate = 9;
	int colWidthHours = 6;
	int colWidthGrossPay = 8;

	cout << "How many employees would you like to enter?\n";
	cin >> arrSize;
	// Dynamically allocate space for an array based on user choice
	employee* emp = new employee[arrSize];

	// iterate through array and store user inputs
	for (int i = 0; i < arrSize; i++) 
	{
		cout << "\nPlease Enter The 4-Digit Employee ID: ";
		cin >> emp[i].employeeID;
		cout << "Enter First Name: ";
		cin >> emp[i].firstName;
		cout << "Enter Last Name: ";
		cin >> emp[i].lastName;
		cout << "Enter Pay Rate: ";
		cin >> emp[i].payRate;
		cout << "Enter hours: ";
		cin >> emp[i].hours;
	}

	for (int i = 0; i < arrSize; i++) {
		//If the size of an employee's name is greater than the default column width, adjust column with to be equal to it + 1 for a space
		if (size(emp[i].firstName) > colWidthFirstName) colWidthFirstName = size(emp[i].firstName) + 1;
		if (size(emp[i].lastName) > colWidthLastName) colWidthLastName = size(emp[i].lastName) + 1;
	}

	
	// Stream modifiers:
	// Set padding direction of field to left, set all floating points in stream to be fixed to 2 decimal places
	cout << left << fixed << setprecision(2);

	// Column Names - setw sets width of field, padding spaces to fill width
	cout << "\n" << setw(colWidthID) << "ID" << setw(colWidthFirstName) << "First Name" << setw(colWidthLastName) << "Last Name" << setw(colWidthPayRate) << "Pay Rate" << setw(colWidthHours) << "Hours" << setw(colWidthGrossPay) << "Gross Pay\n";
	
	colWidthPayRate = colWidthPayRate - 1; // Substract 1 off field width because we add a $ to the stream before pay rate values below

	
	for (int i = 0; i < arrSize; i++)
	{
		// Calculate Gross Pay
		grossPay = emp[i].payRate * emp[i].hours;
		// Column Values
		cout << setw(colWidthID) << emp[i].employeeID 
			<< setw(colWidthFirstName) << emp[i].firstName 
			<< setw(colWidthLastName) << emp[i].lastName 
			<< setw(1) << "$" // Add $ to stream, colWidthPayRate was adjusted -1 to account for it
			<< setw((colWidthPayRate)) << emp[i].payRate
			<< setw(colWidthHours)<< emp[i].hours 
			<< "$" << setw(colWidthGrossPay) << grossPay << "\n";
		totalgrossPay += grossPay;
	}

	cout << "\nTotal Gross Pay: $" << totalgrossPay;

	delete[] emp; // delete array since it is on the heap

	(void)_getch();
	return 0;
}